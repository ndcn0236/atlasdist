"""Implements algorithm to compute distance between point and mask."""

from argparse import ArgumentParser
from typing import Union

import numpy as np
from fsl.data.image import Image
from fsl.transform import affine

__version__ = "0.0.0"


def dist(point, mask_points, voxel_size):
    """
    Compute the distance between `point` and the mask.

    Both `point` and `mask_points` should be in FSL scaled voxel space.
    `voxel_size` should be the size the voxel along each dimension.
    """
    shift_to_center = abs(point - mask_points)
    shift_to_edge = np.maximum(shift_to_center - voxel_size / 2, 0.0)
    dist_sq = np.sum(shift_to_edge**2, -1)
    return np.sqrt(np.amin(dist_sq))


def run(  # noqa: C901
    points: np.ndarray,
    image: Image,
    coordinate_system="world",
    index: Union[None, int] = None,
    threshold: Union[None, float] = None,
):
    """Run the main algorithm from within python."""
    if image.ndim == 4:
        # probablistic atlas
        if index is None:
            print(
                "The input image contains multiple volumes. The following will contain the closest coordinate for the mask in each volume:"
            )
            for sub_index in range(image.shape[-1]):
                print("Volume %i" % sub_index)
                run(points, image, coordinate_system, sub_index, threshold)
            return
        if threshold is None or threshold == 0.0:
            mask = image.data[..., index] > 0
        else:
            mask = image.data[..., index] >= threshold
    else:
        if threshold is not None:
            if index is not None:
                raise ValueError(
                    "Cannot select both a label (`-i/--index`) and a threshold (`-t/--threshold`) for a 3D image."
                )
            if threshold == 0.0:
                mask = image.data > 0
            else:
                mask = image.data >= threshold
        elif np.all(image.data.astype(int) == image.data):
            available_labels = list(np.unique(image.data))
            available_labels.remove(0)
            if len(available_labels) == 1:
                # image only contains 0 and 1; must be a mask
                if index is not None and index != 1:
                    raise ValueError(
                        f"Input appears to be a 3D mask containing only 0 and 1, so did not expect `-i/--index={index}` to be set."
                    )
                mask = image.data == 1
            else:
                # image contains only integers; must be an atlas
                if index is None:
                    print(
                        "The input image contains multiple different labels. The following will contain the closest coordinate for the mask for each label:"
                    )
                    for sub_index in available_labels:
                        print("Label %i" % sub_index)
                        run(points, image, coordinate_system, sub_index)
                    return
                mask = image.data == index
        else:
            mask = image.data > 0
    if mask.sum() == 0:
        raise ValueError(
            "Encountered an empty atlas/mask. Did you select the correct `-i/--index` and/or `-t/--threshold`?"
        )

    mask_voxels = np.array(np.where(mask)).T
    voxel_to_scaled = image.getAffine("voxel", "fsl")
    m_scaled = affine.transform(mask_voxels, voxel_to_scaled)
    voxel_size = np.linalg.norm(voxel_to_scaled, axis=0)[:-1]

    point_to_scaled = image.getAffine(coordinate_system, "fsl")

    print("X, Y, Z, distance (mm)")
    for point in points:

        p_scaled = affine.transform(point, point_to_scaled)
        distance = dist(p_scaled, m_scaled, voxel_size)
        print(*point, distance)


def get_parser(parser=None):
    """Get the command line interface parser."""
    if parser is None:
        parser = ArgumentParser(
            description="Compute distance between a set of points and a mask."
        )
    parser.add_argument(
        "points",
        help="CSV file with coordinates or a single coordinate defined as X,Y,Z.",
    )
    parser.add_argument(
        "mask",
        help="NIFTI image containing a mask or an atlas. If an atlas is used, use `-i/--index` to determine which index to use.",
    )
    parser.add_argument(
        "-v",
        "--voxel",
        action="store_true",
        help="Use this flag if the input coordinates are in voxel rather than world (mm) space.",
    )
    parser.add_argument(
        "-i", "--index", type=int, help="The index of the ROI to use from the atlas."
    )
    parser.add_argument(
        "-t",
        "--threshold",
        type=float,
        help="Threshold to apply to convert image into mask.",
    )
    return parser


def main():
    """Run the algorithm from the command line."""
    parser = get_parser()
    args = parser.parse_args()
    with open(args.points, "r") as f:
        lines = [line.strip("\ufeff") for line in f.readlines()]
    run(
        np.loadtxt(lines, dtype=float, delimiter=","),
        Image(args.mask),
        coordinate_system="voxel" if args.voxel else "world",
        index=args.index,
        threshold=args.threshold,
    )
