[![PyPI - Downloads](https://img.shields.io/pypi/dm/atlasdist)](https://pypi.org/project/atlasdist/)
[![Documentation](https://img.shields.io/badge/Documentation-atlasdist-blue)](https://open.win.ox.ac.uk/pages/ndcn0236/atlasdist)
[![Pipeline status](https://git.fmrib.ox.ac.uk/ndcn0236/atlasdist/badges/main/pipeline.svg)](https://git.fmrib.ox.ac.uk/ndcn0236/atlasdist/-/pipelines/latest)
[![Coverage report](https://git.fmrib.ox.ac.uk/ndcn0236/atlasdist/badges/main/coverage.svg)](https://open.win.ox.ac.uk/pages/ndcn0236/atlasdist/htmlcov)

`atlasdist` computes the distance between a set of points and a mask/atlas.

# Installation
```shell
pip install git+https://git.fmrib.ox.ac.uk/ndcn0236/atlasdist.git
```

After running this command, `atlasdist` will be available for use within the terminal.

Any bug reports and feature requests are very welcome (see [issue tracker](https://git.fmrib.ox.ac.uk/ndcn0236/atlasdist/-/issues)).

# Usage
Both points and mask(s) need to be provided in the same space (e.g., MNI).
Points are input as a CSV file, like:
```
8.12,-38.32,9.87
7.56,-34.12,9.21
```
This file defines two points.
You can create such a file in Microsoft Excel by creating a new table with 3 columns containing just the point coordinates
and then saving this table using the CSV File Format.

The mask or atlas is provided as a NIFTI image.
- For a single mask, the NIFTI image is expected to be a single volume (i.e., a 3D NIFTI image). `-t/--threshold` can be used to threshold the image (recommended if mask contains more than zeros and ones).
- Atlases can be provided in one of two ways:
    - a 4D NIFTI image, where each of the volumes in the NIFTI image is a different ROI. Each ROI can be thresholded using `-t/--threshold` (optional). If you only want the output for a specific ROI, you can use `-i/--index` (optional).
    - a 3D NIFTI image, where each ROI has a different integer label. If you only want output for a specific label, you can use `-i/--index` (optional).

```
usage: atlasdist [-h] [-v] [-i INDEX] [-t THRESHOLD] points mask

Compute distance between a set of points and a mask.

positional arguments:
  points                CSV file with coordinates or a single coordinate defined as X,Y,Z.
  mask                  NIFTI image containing a mask or an atlas. If an atlas is used, use `-i/--index` to determine which index to use.

options:
  -h, --help            show this help message and exit
  -v, --voxel           Use this flag if the input coordinates are in voxel rather than world (mm) space.
  -i INDEX, --index INDEX
                        The index of the ROI to use from the atlas.
  -t THRESHOLD, --threshold THRESHOLD
                        Threshold to apply to convert image into mask.
```
