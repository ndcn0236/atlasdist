"""Main testing file for atlasdist."""
import numpy as np

from atlasdist import dist


def test_dist():
    """Test the low-level `dist` function."""
    for (point, res1, res2) in [
        ([1.0, 0.0, 0.0], 0.5, 0.5),
        ([1.0, 1.0, 2.0], 0.5, 0.0),
        ([1.0, 1.0, 1.0], 0.0, 0.0),
        ([2.0, 2.0, 2.0], np.sqrt(3) / 2, np.sqrt(2) / 2),
        ([1.0, 1.0, 6.0], 4.5, 3.0),
        ([1.2, 0.6, 6.0], 4.5, 3.0),
    ]:
        assert (
            dist(np.array(point), [[1, 1, 1], [1, 1, 0]], np.array([1.0, 1.0, 1.0]))
            == res1
        )
        assert (
            dist(np.array(point), [[1, 1, 2], [1, 1, 0]], np.array([1.0, 1.0, 2.0]))
            == res2
        )
